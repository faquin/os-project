#include "scheduler.h"

extern uint32_t l1_page_table;
extern uint32_t _page_table;
extern uint32_t _page_table_end;

//Try to turn on the MMU
void initialize_mmu(){
    int delta   = 16;

    /*
     * If the first-level descriptor is a coarse page table descriptor, the
     * fields have the following meanings:
     * Bits[1:0] Identify the type of descriptor (0b01 marks a coarse page table
     * descriptor).
     * Bits[4:2] The meaning of these bits is IMPLEMENTATION DEFINED . From
     * VMSAv6 these bits SBZ.
     * Bits[8:5] The domain field specifies one of the 16 possible domains for
     * all the pages controlled by
     * this descriptor.
     * Bit[9] IMPLEMENTATION DEFINED .
     * Bits[31:10] The Page Table Base Address is a pointer to a coarse
     * second-level page table, giving the base
     * address for a second-level fetch to be performed. Coarse second-level
     * page tables must be
     * aligned on a 1KB boundary.
     *
     * Only bits [31:14-N] of the translation table base 0 register are
     * significant. Therefore if N = 0, the page table
     * must reside on a 16KB boundary, and, for example, if N = 1, it must
     * reside on an 8KB boundary.
     */
    uint32_t count =0;

    uint32_t* l1_table = (uint32_t*)(uintptr_t)&l1_page_table;
    int i=0;
    for(i=0; i<MAX_PAGES; i++){
        l1_table[i] = (uint32_t)(i<<20) | READ_WRITE | TYPE_SECTION | CACHABLE;
    }

    //    kprintf("la base %#032x %d\n", l1_table, count);
    //    uint32_t base_l2 = (uint32_t)(uintptr_t)&_page_table;
    //    uint32_t end_l2i = (uint32_t)(uintptr_t)&_page_table_end;
    //    //align on 1K
    //    kprintf("la base %#032x %d\n", base_l2, count);
    //
    //    uint32_t i = 0;
    //    uint32_t real_adress = 0;
    //    for(i=0; i<32; i++){
    //        //There is 1024 pages per 32 first level entry. Process L2 base adress
    //        uint32_t* l2_table = (uint32_t*)(base_l2 + ((i*1024)*4));
    //        kprintf("pouet %d %d %d\n", i, l2_table, end_l2i);
    //        //first level, bit 10 to 31 representing L2 base adress
    //        l1_table[i] = ((((uint32_t)l2_table) ) << 10) | 0x01;
    //        uint32_t j = 0;
    //        for(j=0; j<1024; j++){
    //            //The content of an L2 entry point to the "real" adress base.
    //            //bits 12 to 31.
    //            l2_table[j]  = (real_adress << 12);
    //            l2_table[j] |= 0b001000110010;
    //            real_adress += 4096;
    //        }
    //    }
    _mmu_on();
}
