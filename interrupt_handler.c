#include "interrupt_handler.h"

int nb_event_to_process = 0;
int nb_event_to_recycle = 0;

void init_events(){
    int i=0;
    events_to_process = 0;
    events_to_recycle = make_event();
    kernel_event* last = events_to_recycle;
    nb_event_to_recycle = EVENT_POOL;
    for (i=1; i<nb_event_to_recycle; i++){
        kernel_event* k = make_event();
        append_event(last, k);
        last = k;
    }
}

//Allocate a new event, initalization purposes
kernel_event* make_event(){
    kernel_event* k = kmalloc(sizeof(kernel_event));
    k->operation = 0;
    k->next      = 0;
    return k;
}

//append an event, initalization purposes
void append_event(kernel_event* a, kernel_event* b){
    a->next = b;
}

//append the event k to the recycle list
//k must not be present in the to process list
void terminate_kernel_event(kernel_event* k){
    int i = 0;
    //kprintf("RECYLE !! r %d, p %d\n", nb_event_to_recycle, nb_event_to_process);
    if(nb_event_to_recycle > 0){
        kernel_event* r = events_to_recycle;
        while(r->next != NULL){
            r = r->next;
        }
        r->next = k;
    }else{
        events_to_recycle = k;
    }
    k->operation =0;
    k->next      =0;
    nb_event_to_recycle++;
}

//add the event k at the end of the to process list
//The event must not be in the recycle list
int add_kernel_event(kernel_event* k){
    int i = 0;
    if(nb_event_to_process > 0){
        kernel_event* r = events_to_process;
        for(i=0; i<nb_event_to_process; i++){
            r = r->next;
        }
        r->next = k;
    }else{
        events_to_process = k;
    }
    k->next =0;
    nb_event_to_process++;
}


//Return the oldest kernel event to process
kernel_event* pop_free_kernel_event(){
    //kprintf("CREATE !! r %d, p %d\n", nb_event_to_recycle, nb_event_to_process);
    if(nb_event_to_recycle > 0){
        kernel_event* k = events_to_recycle;
        events_to_recycle = k->next;
        nb_event_to_recycle--;
        return k;
    }else{
        return NULL;
    }
}

//Return the oldest kernel event to process
kernel_event* get_next_kernel_event(){
    if(nb_event_to_process > 0){
        kernel_event* k = events_to_process;
        events_to_process = k->next;
        nb_event_to_process--;
        return k;
    }else{
        return NULL;
    }
}

//Get the first kernel event, process it and recycle it.
void execute_next_kernel_event(){
    kernel_event* k = get_next_kernel_event();
    if(k != NULL){
        arm_enable_interrupts();//TODO temporary code
        ((void(*)()) k->operation)();
        arm_disable_interrupts();//TODO temporary code
        terminate_kernel_event(k);
    }else{
    }
}


///////////////////////////////////////////////////////////////////////////////
//
//                              INTERRUPTIONS
//
///////////////////////////////////////////////////////////////////////////////

void handle_interrupt(irq_id_t irq, cpu_id_t cpu){
  if (irq == UART0_IRQ) {
      uart_top();
  }else if (irq == TIMER0_IRQ){
      timer_top();
  }
}

//UART ------------------------------------------------------------------------
void uart_handler_init(struct pl011_uart* sin, struct pl011_uart* sout){
    stdin  = sin;
    stdout = sout;
    uart_circular_buffer = kmalloc(sizeof(cbuffer));
    uart_circular_buffer->lastRead     = 0;
    uart_circular_buffer->lastWrite    = 0;
    uart_circular_buffer->element_size = sizeof(char);
    uart_circular_buffer->max_size     = MAX_BUFFER_SIZE;
    uart_circular_buffer->free_space   = MAX_BUFFER_SIZE;
    uart_circular_buffer->buffer       = kmalloc(uart_circular_buffer->max_size *
                                                 uart_circular_buffer->element_size);
    /*
     * Enable the RX interrupt on the UART0, our standard input (stdin).
     * We do not need to enable any other interrupts, but many others exist.
     */
    uart_enable_irqs(stdin,UART_IMSC_RXIM);
    cortex_a9_gid_enable_irq(UART0_IRQ);
}

void uart_top(){
    char c = '.';
    /*
     * You must do the read here first, from the UART0, before doing any print
     * on the same serial line... Normally, this should not be necessary!
     * The reason is obscure, it is because of an unexplained GCC behavior.
     * For some unknown reason, GCC generates reads of the UART.DR register when writing to it...
     * which therefore reads the pending character out of the FIFO and looses it.
     * Also, the read may lower the IRQ line, if the read drops the number of pending
     * characters in the receive FIFO below the RX interrupt threshold.
     */
    uart_receive(stdin, &c);
    if( circular_buffer_write(uart_circular_buffer, &c) == 0){
        //TODO create bottom event
        kernel_event* fresh_event = pop_free_kernel_event();
        if(fresh_event != NULL){
            fresh_event->operation = uart_bottom;
            add_kernel_event(fresh_event);
        }else{
            //TODO no more room for an other kernel event
        }
    }else{
        //TODO handle write error
    }
    uart_ack_irqs(stdin);
}

//Write all buffered chars to the output
void uart_bottom(){
    char c = '.';
    while(circular_buffer_read(uart_circular_buffer, &c) == 0){
        if (c == 13) {
            uart_send(stdout, '\r');
            uart_send(stdout, '\n');
        } else {
            uart_send(stdout, c);
        }
    }
}

//Timer -----------------------------------------------------------------------
void timer_handler_init(){
    timer_init();
    timer_set_auto_load(1);
    timer_set_timer(TIMER_SCHEDULING);
    cortex_a9_gid_enable_irq(TIMER0_IRQ);
    timer_enable_irq();
    timer_enable();
}

//Pop an event and execute it
void timer_top(){
    execute_next_kernel_event();
    timer_ack_irq();
}
