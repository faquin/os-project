#ifndef interrupt_handler_h_
#define interrupt_handler_h_

#include "board.h"
#include "pl011.h"
#include "gic.h"
#include "gid.h"
#include "kmem.h"
#include "timer.h"
#include "circular_buffer.h"

//scheduling ------------------------------------------------------------------
#define SCHEDULE_POOL 64;
typedef struct {
    
} schdule_event;

//events ----------------------------------------------------------------------
#define EVENT_POOL 64;
typedef struct {
    void* operation;
    void* next;
} kernel_event;

kernel_event* events_to_process;
kernel_event* events_to_recycle;

void          init_events();
void          append_event(kernel_event* a, kernel_event* b);
kernel_event* make_event();
kernel_event* get_next_kernel_event();
void          terminate_kernel_event(kernel_event* k);
int           add_kernel_event(kernel_event* k);
kernel_event* pop_free_kernel_event();
void          execute_next_kernel_event();

// interruptions ---------------------------------------------------------------
#define MAX_BUFFER_SIZE 64;
cbuffer* uart_circular_buffer;
cbuffer* events;

void handle_interrupt(irq_id_t irq, cpu_id_t cpu);

//UART
struct pl011_uart* stdin;
struct pl011_uart* stdout;
void uart_handler_init(struct pl011_uart*, struct pl011_uart*);
void uart_top();
void uart_bottom();

//Timer
#define TIMER_SCHEDULING 1000000//TODO
void timer_handler_init();
void timer_top();
#endif
