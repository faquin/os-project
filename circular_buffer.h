#ifndef circular_buffer_h_
#define circular_buffer_h_

#include "board.h"

typedef struct {
    void* buffer;
    uint32_t lastWrite;
    uint32_t lastRead;
    uint32_t max_size;
    uint32_t element_size;
    uint32_t free_space;
} cbuffer;

int circular_buffer_write(cbuffer*, void*);
int circular_buffer_read(cbuffer*, void*);

#endif
