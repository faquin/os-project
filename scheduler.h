#ifndef scheduler_h
#define scheduler_h

#include "board.h"
#include "kmem.h"

#define MEMORY_SIZE  128
#define MAX_PAGES    32768

#define READ_WRITE   3 << 10
#define TYPE_SECTION 1 << 1
#define CACHABLE     1 << 3

uint32_t* l1_table;

void initialize_mmu();
void flush_tlb();
void init_mmu();
extern void _mmu_on();
#endif
