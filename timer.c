#include "timer.h"

#ifdef vexpress_a9
static uint32_t periph_base = 0x0;
static uint32_t control_register_bit = 0x0;

void timer_init() {
    periph_base = cortex_a9_peripheral_base();
}

void timer_enable() {
    control_register_bit |= ARM_TIMER_CR_TE;
    arm_mmio_write32(periph_base + ARM_PWT_BASE_OFFSET, ARM_TIMER_CR, control_register_bit);
}

void timer_disable() {
    control_register_bit &= ~(ARM_TIMER_CR_TE);
    arm_mmio_write32(periph_base + ARM_PWT_BASE_OFFSET, ARM_TIMER_CR, control_register_bit);
}

//Enable the irq for the timer
void timer_enable_irq() {
    control_register_bit |= ARM_TIMER_CR_IE;
    arm_mmio_write32(periph_base + ARM_PWT_BASE_OFFSET, ARM_TIMER_CR, control_register_bit);
}

//disable the irq for the timer
void timer_disable_irq() {
    control_register_bit &= ~(ARM_TIMER_CR_IE);
    arm_mmio_write32(periph_base + ARM_PWT_BASE_OFFSET, ARM_TIMER_CR, control_register_bit);
}

//Acknowledge the irq
void timer_ack_irq() {
    // The event flag is cleared when written to 1.
    arm_mmio_write32(periph_base + ARM_PWT_BASE_OFFSET, ARM_TIMER_ISR, 1);
}

//Time is not in seconds, in cycles
void timer_set_timer(int t) {
    arm_mmio_write32(periph_base + ARM_PWT_BASE_OFFSET, ARM_TIMER_LR, t);
}

//Enable autoload or not, depending on s value
//1 enable cycling interruptions
void timer_set_auto_load(int s) {
    if (s)
        control_register_bit |= ARM_TIMER_CR_AR;
    else
        control_register_bit &= ~(ARM_TIMER_CR_AR);
    arm_mmio_write32(periph_base + ARM_PWT_BASE_OFFSET, ARM_TIMER_CR, control_register_bit);
}
#endif
