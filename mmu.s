/**
 * This function turn on the mmu
 */
.global _mmu_on
.func _mmu_on
_mmu_on:
    dsb  @ make sure all updates have reached memory
    isb  @ before the next instruction

    mov r1,#0x0
    mcr p15, 0, r1, c2, c0, 2 @ Write Translation Table Base Control Register
    ldr r1, =l1_page_table
    mcr p15, 0, r1, c2, c0, 0 @ Write Translation Table Base Register 0
    mov    r1, #-1
    orr r0, r0, #0x1          @ enable MMU
    mcr p15, 0, r1, c3, c0, 0 @ Write Domain Access Control Register
    mcr p15, 0, r0, c1, c0, 0 @ Write Control Register configuration data

    isb  @ before the next instruction
    mov pc,lr

.endfunc
