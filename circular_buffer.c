#include "circular_buffer.h"

int circular_buffer_write(cbuffer*buffer, void*data){
    if(buffer->free_space > 0){
        buffer->lastWrite++;
        buffer->lastWrite = buffer->lastWrite % buffer->max_size;
        buffer->free_space--;
        *((char*) buffer->buffer+(buffer->lastWrite*buffer->element_size)) = *((char*)data);
        return 0;
    }else{
        return -1;
    }
}

int circular_buffer_read(cbuffer*buffer, void*data){
    uint32_t free_space = buffer->lastRead - buffer->lastWrite % buffer->max_size;
    if(buffer->free_space < buffer->max_size){
        buffer->lastRead++;
        buffer->lastRead = buffer->lastRead % buffer->max_size;
        buffer->free_space++;
        *((char*) data) = *((char*) buffer->buffer+(buffer->lastRead*buffer->element_size));
        return 0;
    }else{
        return -1;
    }
}


