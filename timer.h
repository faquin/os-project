#ifndef timer_h
#define timer_h

/*
 * Code hardly inspired by Jeremie Finiel's one.
 */
#include "board.h"

#ifdef vexpress_a9

//Cortex A9 mpcore Global timer Page 62 Chapter 4
//Global timer -> increment
//private timer -> a timer per core
//  Watchdog register

#define ARM_TIMER_LR 0x0000		// load register
#define ARM_TIMER_COUR 0x0004	// counter register
#define ARM_TIMER_CR 0x0008 	// control register
#define ARM_TIMER_ISR 0x000C 	// interrupt state register

#define ARM_TIMER_CR_TE (1<<0)	// time enable
#define ARM_TIMER_CR_AR (1<<1)	// auto reload
#define ARM_TIMER_CR_IE (1<<2)	// irq enable

void timer_init();
void timer_enable();
void timer_disable();

void timer_enable_irq();
void timer_disable_irq();
void timer_ack_irq();
void timer_set_timer(int t);
void timer_set_auto_load(int s);

#endif
#endif
